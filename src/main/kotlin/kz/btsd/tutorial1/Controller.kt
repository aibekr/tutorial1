package kz.btsd.tutorial1

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class Controller {
    @GetMapping("/tutorial")
    fun tutorial() = "index"
}