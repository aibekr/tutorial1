FROM openjdk:13-alpine
VOLUME /tmp
COPY build/libs/tutorial1-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]